// Cursor Mouse
function cubeCursor(){
    var x = cursor.x,
    y = cursor.y;

    document.getElementById('cursor').style.left = x +'px';
    document.getElementById('cursor').style.top = y +'px';
}

var timeout;
addEventListener("mousemove", function(event) {
    clearTimeout(timeout);
    timeout = setTimeout(function(){  
        this.document.getElementById('cursor_cube').style.animationPlayState = 'running';
    ;}, 500);
  
  //event.preventDefault();
  cursor.x = event.pageX;
  cursor.y = event.pageY;
  this.document.getElementById('cursor_cube').style.animationPlayState = 'paused';
});

// animate() calls draw() then recursively calls itself
  // everytime the screen repaints via requestAnimationFrame().
function animate() {
  cubeCursor();
  requestAnimationFrame(animate);
}

// And get it started by calling animate().
animate();

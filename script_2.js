import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'

// Debug
const gui = new dat.GUI()

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Objects
const geometry = new THREE.TorusGeometry( .7, .2, 16, 100 );

// Materials

const material = new THREE.MeshBasicMaterial()
material.color = new THREE.Color(0xff0000)

// Mesh
const sphere = new THREE.Mesh(geometry,material)
scene.add(sphere)

// Lines
const s = 5;


// Table Outline
const linemat = new THREE.LineBasicMaterial({
  color: 0xec0db4
});


for(var i = 0; i < 10; i++){
    const points = [];
points.push( new THREE.Vector3( i*5, 5, 0 ) );
points.push( new THREE.Vector3( i*5, 5, 5 ) );

const geoline = new THREE.BufferGeometry().setFromPoints( points );

const line = new THREE.Line( geoline, linemat );
line.matrix.makeRotationFromQuaternion( Math.PI/2 );

scene.add( line );
}
for(var i = 0; i < 5; i++){
    const points2 = [];
points2.push( new THREE.Vector3( -100, 5, i ) );
points2.push( new THREE.Vector3( 100, 5, i ) );

const geoline2 = new THREE.BufferGeometry().setFromPoints( points2 );

const line2 = new THREE.Line( geoline2, linemat );
line2.matrix.makeRotationFromQuaternion( Math.PI/2 );

scene.add( line2 );
}

// Text
const loader = new THREE.FontLoader();

loader.load( 'fonts/Poppins_Regular.js', function ( font ) {
    var textGeometry = new THREE.TextGeometry( "HI", {
  
      font: font,
  
      size: 50,
      height: 10,
      curveSegments: 12,
  
      bevelThickness: 1,
      bevelSize: 1,
      bevelEnabled: true
  
    });
  
    var textMaterial = new THREE.MeshPhongMaterial( 
      { color: 0xff0000, specular: 0xff0000 }
    );

  
    var mesh = new THREE.Mesh( textGeometry, textMaterial );

    console.log('text pos', mesh.position.y);
  
    scene.add( mesh );
  });  


  // Count Mesh
  scene.traverse( function( object ) {

    if ( object.isMesh ) console.log( object );

} );



// 

// Lights

const pointLight = new THREE.PointLight(0xffffff, 0.1)
pointLight.position.x = 2
pointLight.position.y = 3
pointLight.position.z = 4
scene.add(pointLight)

// Light in Tunnel
var light = new THREE.PointLight(0xffffff,1, 50);
scene.add(light);	

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(200, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 0
camera.position.y = 0
camera.position.z = 10
// camera.rotation.x = Math.PI/4
scene.add(camera)

// Controls
// const controls = new OrbitControls(camera, canvas)
// controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

scene.background = new THREE.Color( 0x000000 );


/**
 * Animate
 */

const clock = new THREE.Clock()

// Scroll 
const mouse = new THREE.Vector2()
var lastY;
document.addEventListener('mousewheel', function(e){
    e.preventDefault();
    mouse.y = - (e.deltaY / window.innerWidth) * 2 + 1;
    camera.position.x -= mouse.y * 0.5;

    if(lastY < mouse.y){
        camera.position.x -= mouse.y * 0.5;
        console.log('cam scroll up', mouse.y);
        document.getElementById('info').classList.add('active');
    }
    else if(lastY > mouse.y) {
        camera.position.x += mouse.y * 0.5;
        console.log('cam scroll down', mouse.y);

    }
    lastY = mouse.y;

    // CSS Animation
    console.log('cam pos x', camera.position.x);
    if(camera.position.x <=0 && camera.position.x >= -3){
        document.getElementById('info').classList.add('active');
    }
    else{
        document.getElementById('info').classList.remove('active');

    }
});


const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()
    // Update objects
    sphere.rotation.y = .5 * elapsedTime
    // Update Orbital Controls
    // controls.update()
    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)

    // Make the cubes fly
    
}

tick();

// Tunnel
var points = [
  [48, 0],
  [52, 5],
  [-20, 10],
  [-30, 2],
  ];

  var camera_points = [
    [48, 0],
    [52, 5],
    [-20, 10],
    // [-30, 5],
    // [48, 0]

  ];  

  //Convert the array of points into vertices
  for (var i = 0; i < points.length; i++) {
    var x = points[i][0];
    var y = 5;
    var z = points[i][1];
    points[i] = new THREE.Vector3(x, y, z);
  }

    //Convert the array of points into vertices
    for (var i = 0; i < camera_points.length; i++) {
      var x = camera_points[i][0];
      var y = 5;
      var z = camera_points[i][1];
      camera_points[i] = new THREE.Vector3(x, y, z);
    }
    
  // Create a path from the points
  var path = new THREE.CatmullRomCurve3(points);	
  // Create a path for camera view
  var camera_path = new THREE.CatmullRomCurve3(camera_points);	

  var geo_tunnel = new THREE.TubeGeometry( path, 64, 2, 8, false );
//Basic red material
var mat_tunnel = new THREE.MeshBasicMaterial({
    color: 0xfffffff, //Red color
    side : THREE.BackSide, //Reverse the sides
    wireframe:true //Display the tube as a wireframe
  });
  	//Create a mesh
var tube = new THREE.Mesh( geo_tunnel, mat_tunnel );
//Add tube into the scene
scene.add( tube );	

// Renderring Tunnel
var percentage = 0;
function render(){
  //Increase the percentage
  percentage += 0.002;
  //Get the point at the specific percentage
  var p1 = camera_path.getPointAt(percentage%1);
    //Get another point along the path but further
    var p2 = camera_path.getPointAt((percentage + 0.01)%1);

    if(percentage >= 0.5){
      p1 = camera_path.getPointAt(0);
      p2 = camera_path.getPointAt(0.5);
    }

    camera.position.set(p1.x,p1.y,p1.z);
    // Rotate the camera into the orientation of the second point
    camera.lookAt(p2);
    // light.position.set(p2.x, p2.y, p2.z)

  renderer.render(scene, camera);
  requestAnimationFrame(render);
}
requestAnimationFrame(render);	



// Mouse
var cursor;
var dots = [],
cursor = {
      x: 0,
      y: 0
    };

// The Dot object used to scaffold the dots
var Dot = function() {
  this.x = 0;
  this.y = 0;
  this.node = (function(){
    var n = document.createElement("div");
    n.className = "trail";
    document.body.appendChild(n);
    return n;
  }());
};
// The Dot.prototype.draw() method sets the position of 
  // the object's <div> node
Dot.prototype.draw = function() {
  this.node.style.left = this.x + "px";
  this.node.style.top = this.y + "px";
};

// Creates the Dot objects, populates the dots array
for (var i = 0; i < 4; i++) {
  var d = new Dot();
  dots.push(d);
}

// This is the screen redraw function
function draw() {
  // Make sure the mouse position is set everytime
    // draw() is called.
  var x = cursor.x,
      y = cursor.y;
  
  // This loop is where all the 90s magic happens
  dots.forEach(function(dot, index, dots) {
    var nextDot = dots[index + 1] || dots[0];
    
    dot.x = x;
    dot.y = y;

    // Change size progressively
    dot.draw();
    x += (nextDot.x - dot.x) * .6;
    y += (nextDot.y - dot.y) * .6;

  });
}

const boxes = document.querySelectorAll('.trail');
var lastBox;
let color = ['red', 'purple', 'yellow']
for (var i = 0; i > boxes.length; i++) {
    boxes[i].style.width = (e.clientX - 50)+'px';
    boxes.style.backgroundColor = color[i%3];

    lastBox = boxes[i].style.width
}

// boxes.forEach(box => {
//   box.style.backgroundColor = 'purple';
// });

addEventListener("mousemove", function(event) {
  //event.preventDefault();
  cursor.x = event.pageX;
  cursor.y = event.pageY;
});

// animate() calls draw() then recursively calls itself
  // everytime the screen repaints via requestAnimationFrame().
function animate() {
  draw();
  requestAnimationFrame(animate);
}

// And get it started by calling animate().
animate();

// Background
// Create star object
const geo_cubes = new THREE.BoxGeometry( 10, 10, 10);
let object;

for ( let i = 0; i < 2000; i ++ ) {

  object = new THREE.Mesh( geo_cubes, new THREE.MeshLambertMaterial( { color: Math.random() * 0xffffff } ) );


  object.position.x = Math.random() * 800 - 400;
  object.position.y = Math.random() * 800 - 400;
  object.position.z = Math.random() * 800 - 400;

  // const positions = new Float32BufferAttribute(object.position.x, object.position.y, object.position.z);

  // geo_cubes.setAttribute('rotation_cube', positions)

  object.rotation.x = Math.random() * 2 * Math.PI;
  object.rotation.y = Math.random() * 2 * Math.PI;
  object.rotation.z = Math.random() * 2 * Math.PI;

  object.scale.x = Math.random() + 0.5;
  object.scale.y = Math.random() + 0.5;
  object.scale.z = Math.random() + 0.5;


  scene.add( object );

}

// Make the cubes float
const floatCubes = () =>
{
    const elapsedTime = clock.getElapsedTime()
    // Update objects
    // Update Orbital Controls
    // controls.update()

    object.rotation.x += 0.0005;
    object.rotation.y += 0.001;

    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(floatCubes)

    // Make the cubes fly
    
}

floatCubes();

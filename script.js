import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import * as dat from 'dat.gui'
import { TessellateModifier } from './modules/TesselateModifier'
import { vertShader, fragShader, uniforms } from 'three/examples/jsm/controls/OrbitControls.js'

// Canvas
const canvas = document.querySelector('canvas.webgl')

// Scene
const scene = new THREE.Scene()

// Start of Parallel Lines
const linemat = new THREE.LineBasicMaterial({
  color: 0x378278
});

// Horizontal lines
for(var i = 0; i < 40; i++){
  const points = [];
points.push( new THREE.Vector3( i*4 -80, -5, 20 ) );
points.push( new THREE.Vector3( i*4 -80, -5, -70 ) );

const geoline = new THREE.BufferGeometry().setFromPoints( points );

const line = new THREE.Line( geoline, linemat );
line.matrix.makeRotationFromQuaternion( Math.PI/2 );

scene.add( line );
}

// Vertical lines
for(var i = 0; i < 50; i++){
  const points2 = [];
points2.push( new THREE.Vector3( -80, -5, i - 25 ) );
points2.push( new THREE.Vector3( 80, -5, i - 25) );

const geoline2 = new THREE.BufferGeometry().setFromPoints( points2 );

const line2 = new THREE.Line( geoline2, linemat );
line2.matrix.makeRotationFromQuaternion( Math.PI/2 );

// scene.add( line2 );
}

// End of Parallel Lines


// Separator of Tunnel & Road
const geo_plane = new THREE.PlaneGeometry( 500, 500 );
const mat_plane = new THREE.MeshBasicMaterial( {color: 0x0000000, side: THREE.DoubleSide} );
const plane = new THREE.Mesh( geo_plane, mat_plane );
plane.position.set(0,0,25);
scene.add( plane );

// Menu Elements
const element_geo_plane = new THREE.PlaneGeometry( 500, 500 );
const element_mat_plane = new THREE.MeshBasicMaterial( {color: 0x0f0000, side: THREE.DoubleSide} );
const element_plane = new THREE.Mesh( element_geo_plane, element_mat_plane );
element_plane.position.set(0,4,-10);
element_plane.rotation.x = Math.PI/2;
// scene.add( element_plane );


  // Count Mesh
  scene.traverse( function( object ) {

    if ( object.isMesh ) console.log( object );

} );

// Lights

const pointLight = new THREE.PointLight(0xffffff, 1)
pointLight.position.x = 2
pointLight.position.y = 3
pointLight.position.z = 4
scene.add(pointLight)

// Directional Lights
const pointLight2 = new THREE.PointLight(0xff0000, 1)
pointLight.position.x = 0
pointLight.position.y = 0
pointLight.position.z = 4
scene.add(pointLight2)

// Light inside Tunnel
var light = new THREE.PointLight(0xffffff,1, 50);
scene.add(light);	

/**
 * Sizes
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})

/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(100, sizes.width / sizes.height, 0.1, 100)
camera.position.x = 0
camera.position.y = -10
camera.position.z = 10
// camera.rotation.x = Math.PI/4
scene.add(camera)

// Orbit Controls
// const controls = new OrbitControls(camera, canvas)
// controls.enableDamping = true

/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))

scene.background = new THREE.Color( 0x000000 );



/**
 * Animate
 */

const clock = new THREE.Clock()
// CSS Scrolling
if(document.getElementById('info').style.top == '50%'){
  console.log('time to change')
};
console.log('TOP', document.getElementById('info').currentStyle);

// Scroll 
const mouse = new THREE.Vector2()
document.addEventListener('mousewheel', function(e){
  document.getElementById('info').color = 'red';

  e.preventDefault();
  // mouse.y = - (e.deltaY / window.innerWidth) * 2 + 1;
  mouse.y = e.deltaY * 0.01;

  camera.position.z -= mouse.y * 0.5;

  // CSS Animation
  console.log('cam pos x', camera.position.x);
  if(camera.position.z <=0 && camera.position.x >= -3){
      document.getElementById('info').classList.add('active');
      document.getElementById('info_info').classList.add('active');

  }
  else{
      document.getElementById('info').classList.remove('active');

  }

  if(camera.position.z <= -10){
    explodingCube();
  }
}, {passive: false});


const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()

    // Update Orbital Controls
    // controls.update()
    // Render
    renderer.render(scene, camera)

    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
    
}

tick();

// Tunnel
var points = [
  [0, 50],
  [-5,40],
  // [10, 30],
  [0, 30]
  ];

  var camera_points = [
    [0, 50],
    [-5, 40],
    [0, 30]
    // [-30, 5],
    // [48, 0]
  ];  

  //Convert the array of points into vertices
  for (var i = 0; i < points.length; i++) {
    var x = points[i][0];
    var y = 0;
    var z = points[i][1];
    points[i] = new THREE.Vector3(x, y, z);
  }

    //Convert the array of points into vertices
    for (var i = 0; i < camera_points.length; i++) {
      var x = camera_points[i][0];
      var y = 0;
      var z = camera_points[i][1];
      camera_points[i] = new THREE.Vector3(x, y, z);
    }
    
  // Create a path from the points
  var path = new THREE.CatmullRomCurve3(points);	
  // Create a path for camera view
  var camera_path = new THREE.CatmullRomCurve3(camera_points);	

  var geo_tunnel = new THREE.TubeGeometry( path, 10, 2, 8, false );
//Basic red material
var mat_tunnel = new THREE.MeshBasicMaterial({
    color: 0xfffffff, //Red color
    side : THREE.BackSide, //Reverse the sides
    wireframe:true //Display the tube as a wireframe
  });
  	//Create a mesh
var tube = new THREE.Mesh( geo_tunnel, mat_tunnel );
//Add tube into the scene
scene.add( tube );	

// Renderring Tunnel
var percentage = 0;
var finishRun = false;
function render(){
  //Increase the percentage
  percentage += 0.004;
  //Get the point at the specific percentage
  var p1 = camera_path.getPointAt(percentage % 1);
  if(percentage >= 1){
    finishRun = true;
  }
    //Get another point along the path but further
    var p2 = camera_path.getPointAt((percentage + 0.01)%1);

    camera.position.set(p1.x,p1.y,p1.z);

    // Rotate the camera into the orientation of the second point
    camera.lookAt(p2);
    // light.position.set(p2.x, p2.y, p2.z)

  renderer.render(scene, camera);
  if(!finishRun){
    requestAnimationFrame(render);
  }
  else if(finishRun){
        camera.position.set(0,0,20);
        camera.rotation.set(0,0,0);

  }
}
requestAnimationFrame(render);	


// Make exploding cube coming into screen
let exploding_cube;

const exploding_cube_geo = new THREE.BoxGeometry( 10, 10, 10);

  exploding_cube = new THREE.Mesh( exploding_cube_geo, new THREE.MeshLambertMaterial( { color: 0xfff000 } ) );
  exploding_cube.position.set(0, 0, -100);

  scene.add( exploding_cube );

  const explodingCube = () => {
    const elapsedTime = clock.getElapsedTime()
    exploding_cube.rotation.x += 0.05;
    exploding_cube.position.z += 0.05;
  
  
  
    window.requestAnimationFrame(explodingCube)
  }

// Background with a lot of Cubes
const geo_cubes = new THREE.BoxGeometry( 10, 10, 10);
let object;
let objects = [];

for ( let i = 0; i < 2000; i ++ ) {
  object = new THREE.Mesh( geo_cubes, new THREE.MeshLambertMaterial( { color: 0x378278 } ) );
  objects.push(object);
  object.position.x = Math.random() * 800 - 400;
  object.position.y = Math.random() * 800 - 400;
  object.position.z = Math.random() * 800 - 100;

  object.rotation.x = Math.random() * 2 * Math.PI;
  object.rotation.y = Math.random() * 2 * Math.PI;
  object.rotation.z = Math.random() * 2 * Math.PI;

  object.scale.x = Math.random() + 0.5;
  object.scale.y = Math.random() + 0.5;
  object.scale.z = Math.random() + 0.5;


  scene.add( object );
}

// Make the cubes on the background float
const floatCubes = () =>
{
    const elapsedTime = clock.getElapsedTime()
    // Update objects
    // Update Orbital Controls
    // controls.update()

    object.rotation.x += 0.0005;
    object.rotation.y += 0.001;

    // Call tick again on the next frame
    window.requestAnimationFrame(floatCubes)

    // Make the cubes fly
    objects.forEach(object => {
      object.position.x += Math.random(0, elapsedTime) *.05;
      object.rotation.x += Math.random(0, elapsedTime) *.005;
    })
    
}
floatCubes();

// Team Members Cubes
// Background with a lot of Cubes
const team_cubes = new THREE.BoxGeometry( 10, 10, 10);
let team_object;
let team_objects = [];

for ( let i = 0; i < 10; i ++ ) {
  team_object = new THREE.Mesh( geo_cubes, new THREE.MeshLambertMaterial( { color: 0xff0000 } ) );
  team_objects.push(team_object);
  team_object.position.x = Math.random() * 800 - 400;
  team_object.position.y = Math.random() * 800 - 400;
  team_object.position.z = Math.random() * 800 - 100;

  team_object.rotation.x = Math.random() * 2 * Math.PI;
  team_object.rotation.y = Math.random() * 2 * Math.PI;
  team_object.rotation.z = Math.random() * 2 * Math.PI;

  team_object.scale.x = Math.random() + 0.5;
  team_object.scale.y = Math.random() + 0.5;
  team_object.scale.z = Math.random() + 0.5;


  scene.add( team_object );
}

// Make the cubes on the background float
const floatTeamCubes = () =>
{
    const elapsedTime = clock.getElapsedTime()
    // Update objects
    // Update Orbital Controls
    // controls.update()

    team_object.rotation.x += 0.0005;
    team_object.rotation.y += 0.001;

    // Call tick again on the next frame
    window.requestAnimationFrame(floatCubes)

    // Make the cubes fly
    team_objects.forEach(object => {
      object.position.x += Math.random(0, elapsedTime) *.05;
      object.rotation.x += Math.random(0, elapsedTime) *.005;
    })
    
}
floatTeamCubes();

// Explosion
//////////////settings/////////
var movementSpeed = 80;
var totalObjects = 1000;
var objectSize = 10;
var sizeRandomness = 4000;
var colors = [0xFF0FFF, 0xCCFF00, 0xFF000F, 0x996600, 0xFFFFFF];
/////////////////////////////////
var dirs = [];
var parts = [];
var container = document.createElement('div');
document.body.appendChild( container );

function ExplodeAnimation(x,y)
{
  var geometry = new THREE.BufferGeometry();

  // var geometry = new THREE.Geometry();
  
  for (i = 0; i < totalObjects; i ++) 
  { 
    var vertex = new THREE.Vector3();
    vertex.x = x;
    vertex.y = y;
    vertex.z = 0;
  
    // geometry.vertices.push( vertex );
    geometry.setAttribute('position', new THREE.BufferAttribute( vertex, 3))
    dirs.push({x:(Math.random() * movementSpeed)-(movementSpeed/2),y:(Math.random() * movementSpeed)-(movementSpeed/2),z:(Math.random() * movementSpeed)-(movementSpeed/2)});
  }
  var material = new THREE.PointsMaterial( { size: objectSize,  color: colors[Math.round(Math.random() * colors.length)] });
  var particles = new THREE.Points( geometry, material );
  
  this.object = particles;
  this.status = true;
  
  this.xDir = (Math.random() * movementSpeed)-(movementSpeed/2);
  this.yDir = (Math.random() * movementSpeed)-(movementSpeed/2);
  this.zDir = (Math.random() * movementSpeed)-(movementSpeed/2);
  
  scene.add( this.object  ); 
  console.log('obj', this.object.position.y)
  
  this.update = function(){
    if (this.status == true){
      var pCount = totalObjects;
      while(pCount--) {
        var particle =  this.object.position
        particle.y += dirs[pCount].y;
        particle.x += dirs[pCount].x;
        particle.z += dirs[pCount].z;
      }
      this.object.geometry.verticesNeedUpdate = true;
    }
  }
  
}


parts.push(new ExplodeAnimation(0, 0));
renderExplosion();

			function renderExplosion() {
        requestAnimationFrame( render );
         
        var pCount = parts.length;
          while(pCount--) {
            parts[pCount].update();
          }

				renderer.render( scene, camera );
      }

window.addEventListener('mousedown', function(e){
  e.preventDefault();
  parts.push(new ExplodeAnimation((Math.random() * sizeRandomness)-(sizeRandomness/2), (Math.random() * sizeRandomness)-(sizeRandomness/2)));
  console.log('parts', parts);
}, false)

// Debug
function gui() {
  // Debug
  const gui = new dat.GUI()
      // let modelFolder = gui.addFolder('3DModel')
      // modelFolder.add(mod.position, 'z', 0, Math.PI * 2)
      // modelFolder.open()
      
  let camFolder = gui.addFolder('Camera')
  camFolder.add(camera.position, 'x', -20, 20).listen();

  camFolder.add(camera.position, 'y', -20, 20).listen();

  camFolder.add(camera.position, 'z', -20, 20).listen();

  camFolder.add(camera.rotation, 'x', -20, 20).listen();

  camFolder.add(camera.rotation, 'y', -20, 20).listen();

  camFolder.add(camera.rotation, 'z', -20, 20).listen();
  camFolder.open()
}
gui();
